// -------------
// TestGraph.c++
// -------------

// https://www.boost.org/doc/libs/1_73_0/libs/graph/doc/index.html

// --------
// includes
// --------

#include <iostream> // cout, endl
#include <iterator> // ostream_iterator
#include <sstream>  // ostringstream
#include <utility>  // pair

#include "boost/graph/adjacency_list.hpp" // adjacency_list

#include "gtest/gtest.h"

#include "Graph.hpp"

// ------
// usings
// ------

using namespace std;
using namespace testing;

// ---------
// TestGraph
// ---------

template <typename G>
struct GraphFixture : Test
{
    // ------
    // usings
    // ------

    using graph_type = G;
    using vertex_descriptor = typename G::vertex_descriptor;
    using edge_descriptor = typename G::edge_descriptor;
    using vertex_iterator = typename G::vertex_iterator;
    using edge_iterator = typename G::edge_iterator;
    using adjacency_iterator = typename G::adjacency_iterator;
    using vertices_size_type = typename G::vertices_size_type;
    using edges_size_type = typename G::edges_size_type;
};

// directed, sparse, unweighted
// possibly connected
// possibly cyclic
using graph_types =
    Types<
        boost::adjacency_list<boost::setS, boost::vecS, boost::directedS>,
        Graph // uncomment
        >;

#ifdef __APPLE__
TYPED_TEST_CASE(GraphFixture, graph_types, );
#else
TYPED_TEST_CASE(GraphFixture, graph_types);
#endif

TYPED_TEST(GraphFixture, add_existing_vertice)
{
    using graph_type = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using vertices_size_type = typename TestFixture::vertices_size_type;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);

    vertex_descriptor vd = vertex(0, g);
    ASSERT_EQ(vd, vdA);

    vertices_size_type vs = num_vertices(g);
    ASSERT_EQ(vs, 1u);
}

TYPED_TEST(GraphFixture, add_edge_find_source_and_target)
{
    using graph_type = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using edge_descriptor = typename TestFixture::edge_descriptor;
    using edges_size_type = typename TestFixture::edges_size_type;
    using vertices_size_type = typename TestFixture::vertices_size_type;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);

    edge_descriptor edAB = add_edge(vdA, vdB, g).first;

    pair<edge_descriptor, bool> p1 = add_edge(vdA, vdB, g);
    ASSERT_EQ(p1.first, edAB);
    ASSERT_EQ(p1.second, false);

    edges_size_type es = num_edges(g);
    ASSERT_EQ(es, 1u);

    vertices_size_type vs = num_vertices(g);
    ASSERT_EQ(vs, 2u);

    vertex_descriptor vd1 = source(edAB, g);
    ASSERT_EQ(vd1, vdA);

    vertex_descriptor vd2 = target(edAB, g);
    ASSERT_EQ(vd2, vdB);
}

TYPED_TEST(GraphFixture, vertex_descriptors_not_equal)
{
    using graph_type = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);

    ASSERT_NE(vdA, vdB);

    ASSERT_EQ(vdA, 0);
    ASSERT_EQ(vdB, 1);
}

TYPED_TEST(GraphFixture, edge_descriptors_not_equal)
{
    using graph_type = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using edge_descriptor = typename TestFixture::edge_descriptor;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);

    pair<edge_descriptor, bool> edAB = add_edge(vdA, vdB, g);
    pair<edge_descriptor, bool> edAC = add_edge(vdA, vdC, g);

    ASSERT_NE(edAB.first, edAC.first);
}

TYPED_TEST(GraphFixture, vertex_iterator)
{
    using graph_type = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using vertex_iterator = typename TestFixture::vertex_iterator;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);

    pair<vertex_iterator, vertex_iterator> p = vertices(g);
    vertex_iterator b = p.first;
    vertex_iterator e = p.second;
    ASSERT_NE(b, e);

    vertex_descriptor vd1 = *b;
    ASSERT_EQ(vd1, vdA);
    ++b;
    ASSERT_NE(b, e);

    vertex_descriptor vd2 = *b;
    ASSERT_EQ(vd2, vdB);
    ++b;
    ASSERT_EQ(b, e);
}

TYPED_TEST(GraphFixture, edge_iterator)
{
    using graph_type = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using edge_descriptor = typename TestFixture::edge_descriptor;
    using edge_iterator = typename TestFixture::edge_iterator;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);

    edge_descriptor edAB = add_edge(vdA, vdB, g).first;
    edge_descriptor edAC = add_edge(vdA, vdC, g).first;

    pair<edge_iterator, edge_iterator> p = edges(g);
    edge_iterator b = p.first;
    edge_iterator e = p.second;
    ASSERT_NE(b, e);

    edge_descriptor ed1 = *b;
    ASSERT_EQ(ed1, edAB);
    ++b;
    ASSERT_NE(b, e);

    edge_descriptor ed2 = *b;
    ASSERT_EQ(ed2, edAC);
    ++b;
    ASSERT_EQ(e, b);
}

TYPED_TEST(GraphFixture, find_edge)
{
    using graph_type = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using edge_descriptor = typename TestFixture::edge_descriptor;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);

    edge_descriptor edAB = add_edge(vdA, vdB, g).first;

    pair<edge_descriptor, bool> found_edge = edge(vdA, vdB, g);
    // cout << "found edge id return value: " << found_edge.first << endl;
    ASSERT_EQ(found_edge.first, edAB);
    ASSERT_EQ(found_edge.second, true);

    pair<edge_descriptor, bool> unfound_edge = edge(vdB, vdC, g);
    // cout << "Unfound edge id return value: " << unfound_edge.first << endl;
    // ASSERT_EQ(unfound_edge.first, );
    ASSERT_EQ(unfound_edge.second, false);
}

TYPED_TEST(GraphFixture, adjacency_iterator)
{
    using graph_type = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using adjacency_iterator = typename TestFixture::adjacency_iterator;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);

    add_edge(vdA, vdB, g);
    add_edge(vdA, vdC, g);

    pair<adjacency_iterator, adjacency_iterator> p = adjacent_vertices(vdA, g);
    adjacency_iterator b = p.first;
    adjacency_iterator e = p.second;
    ASSERT_NE(b, e);

    vertex_descriptor vd1 = *b;
    ASSERT_EQ(vd1, vdB);
    ++b;
    ASSERT_NE(b, e);

    vertex_descriptor vd2 = *b;
    ASSERT_EQ(vd2, vdC);
    ++b;
    ASSERT_EQ(e, b);
}

TYPED_TEST(GraphFixture, edge_test)
{
    using graph_type = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using edge_descriptor = typename TestFixture::edge_descriptor;
    using edges_size_type = typename TestFixture::edges_size_type;

    graph_type g; // directed graph

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);

    edge_descriptor edAB = add_edge(vdA, vdB, g).first;

    pair<edge_descriptor, bool> p1 = add_edge(vdA, vdB, g);
    ASSERT_EQ(p1.first, edAB);
    ASSERT_EQ(p1.second, false);

    pair<edge_descriptor, bool> p2 = edge(vdA, vdB, g);
    ASSERT_EQ(p2.first, edAB);
    ASSERT_EQ(p2.second, true);

    edges_size_type es = num_edges(g);
    ASSERT_EQ(es, 1u);

    vertex_descriptor vd1 = source(edAB, g);
    ASSERT_EQ(vd1, vdA);

    vertex_descriptor vd2 = target(edAB, g);
    ASSERT_EQ(vd2, vdB);
}

TYPED_TEST(GraphFixture, test0)
{
    using graph_type = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using vertices_size_type = typename TestFixture::vertices_size_type;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);

    vertex_descriptor vd = vertex(0, g);
    ASSERT_EQ(vd, vdA);

    vertices_size_type vs = num_vertices(g);
    ASSERT_EQ(vs, 1u);
}

TYPED_TEST(GraphFixture, test1)
{
    using graph_type = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using edge_descriptor = typename TestFixture::edge_descriptor;
    using edges_size_type = typename TestFixture::edges_size_type;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);

    edge_descriptor edAB = add_edge(vdA, vdB, g).first;

    pair<edge_descriptor, bool> p1 = add_edge(vdA, vdB, g);
    ASSERT_EQ(p1.first, edAB);
    ASSERT_EQ(p1.second, false);

    pair<edge_descriptor, bool> p2 = edge(vdA, vdB, g);
    ASSERT_EQ(p2.first, edAB);
    ASSERT_EQ(p2.second, true);

    edges_size_type es = num_edges(g);
    ASSERT_EQ(es, 1u);

    vertex_descriptor vd1 = source(edAB, g);
    ASSERT_EQ(vd1, vdA);

    vertex_descriptor vd2 = target(edAB, g);
    ASSERT_EQ(vd2, vdB);
}

TYPED_TEST(GraphFixture, test2)
{
    using graph_type = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using vertex_iterator = typename TestFixture::vertex_iterator;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);

    pair<vertex_iterator, vertex_iterator> p = vertices(g);
    vertex_iterator b = p.first;
    vertex_iterator e = p.second;
    ASSERT_NE(b, e);

    vertex_descriptor vd1 = *b;
    ASSERT_EQ(vd1, vdA);
    ++b;
    ASSERT_NE(b, e);

    vertex_descriptor vd2 = *b;
    ASSERT_EQ(vd2, vdB);
    ++b;
    ASSERT_EQ(b, e);
}

TYPED_TEST(GraphFixture, test3)
{
    using graph_type = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using edge_descriptor = typename TestFixture::edge_descriptor;
    using edge_iterator = typename TestFixture::edge_iterator;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);

    edge_descriptor edAB = add_edge(vdA, vdB, g).first;
    edge_descriptor edAC = add_edge(vdA, vdC, g).first;

    pair<edge_iterator, edge_iterator> p = edges(g);
    edge_iterator b = p.first;
    edge_iterator e = p.second;
    ASSERT_NE(b, e);

    edge_descriptor ed1 = *b;
    ASSERT_EQ(ed1, edAB);
    ++b;
    ASSERT_NE(b, e);

    edge_descriptor ed2 = *b;
    ASSERT_EQ(ed2, edAC);
    ++b;
    ASSERT_EQ(e, b);
}

TYPED_TEST(GraphFixture, test4)
{
    using graph_type = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using adjacency_iterator = typename TestFixture::adjacency_iterator;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);

    add_edge(vdA, vdB, g);
    add_edge(vdA, vdC, g);

    pair<adjacency_iterator, adjacency_iterator> p = adjacent_vertices(vdA, g);
    adjacency_iterator b = p.first;
    adjacency_iterator e = p.second;
    ASSERT_NE(b, e);

    vertex_descriptor vd1 = *b;
    ASSERT_EQ(vd1, vdB);
    ++b;
    ASSERT_NE(b, e);

    vertex_descriptor vd2 = *b;
    ASSERT_EQ(vd2, vdC);
    ++b;
    ASSERT_EQ(e, b);
}

// // add edge tests
TYPED_TEST(GraphFixture, add_edge0)
{
    using graph_type = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using edge_descriptor = typename TestFixture::edge_descriptor;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);

    pair<edge_descriptor, bool> p1 = add_edge(vdA, vdB, g);
    pair<edge_descriptor, bool> p2 = add_edge(vdA, vdB, g);

    ASSERT_TRUE(p1.second);
    ASSERT_FALSE(p2.second);
}

// add edge tests
TYPED_TEST(GraphFixture, add_edge1)
{
    using graph_type = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using edge_descriptor = typename TestFixture::edge_descriptor;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);

    pair<edge_descriptor, bool> p1 = add_edge(vdA, vdB, g);
    pair<edge_descriptor, bool> p2 = add_edge(vdB, vdA, g);
    pair<edge_descriptor, bool> p3 = add_edge(vdB, vdA, g);

    ASSERT_NE(p1.first, p2.first);
    ASSERT_TRUE(p1.second);
    ASSERT_TRUE(p2.second);
    ASSERT_EQ(p2.first, p3.first);
    ASSERT_FALSE(p3.second);
}

// add edge tests
TYPED_TEST(GraphFixture, add_edge3)
{
    using graph_type = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using adjacency_iterator = typename TestFixture::adjacency_iterator;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vertex_array[200];
    for (int i = 0; i < 200; ++i)
    {
        vertex_array[i] = add_vertex(g);
        add_edge(vdA, vertex_array[i], g);
    }

    pair<adjacency_iterator, adjacency_iterator> adj = adjacent_vertices(vdA, g);
    int count = 0;
    adjacency_iterator b = adj.first;
    adjacency_iterator e = adj.second;
    while (b != e)
    {
        ++count;
        ++b;
    }

    ASSERT_EQ(num_edges(g), 200);
    ASSERT_EQ(count, 200);
}

// add vertex test
TYPED_TEST(GraphFixture, add_vertex0)
{
    using graph_type = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;

    graph_type g;

    vertex_descriptor vertex_array[200];
    for (int i = 0; i < 200; ++i)
    {
        vertex_array[i] = add_vertex(g);
        add_edge(vertex_array[i], vertex_array[i / 2], g);
        add_edge(vertex_array[i / 2], vertex_array[i], g);
    }

    ASSERT_EQ(num_vertices(g), 200);
}

TYPED_TEST(GraphFixture, add_vertex1)
{
    using graph_type = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;

    graph_type g;

    for (int i = 0; i < 10; ++i)
    {
        vertex_descriptor vdA = add_vertex(g);
        ASSERT_EQ(vdA, i);
    }
}

// adjacent vertices
TYPED_TEST(GraphFixture, adj_vertex0)
{
    using graph_type = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using adjacency_iterator = typename TestFixture::adjacency_iterator;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    pair<adjacency_iterator, adjacency_iterator> adj = adjacent_vertices(vdA, g);

    ASSERT_EQ(adj.first, adj.second);
}

TYPED_TEST(GraphFixture, adj_vertex1)
{
    using graph_type = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using adjacency_iterator = typename TestFixture::adjacency_iterator;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);

    add_edge(vdA, vdB, g);
    add_edge(vdB, vdA, g);

    pair<adjacency_iterator, adjacency_iterator> adj = adjacent_vertices(vdA, g);
    adj.first++;
    ASSERT_EQ(adj.first, adj.second);
}

TYPED_TEST(GraphFixture, adj_vertex2)
{
    using graph_type = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using adjacency_iterator = typename TestFixture::adjacency_iterator;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);
    vertex_descriptor vdD = add_vertex(g);

    add_edge(vdA, vdB, g);
    add_edge(vdA, vdC, g);
    add_edge(vdA, vdD, g);

    pair<adjacency_iterator, adjacency_iterator> adj = adjacent_vertices(vdA, g);
    adjacency_iterator b = adj.first;
    adjacency_iterator e = adj.second;
    int count = 0;
    while (b != e)
    {
        ++count;
        ++b;
    }
    ASSERT_EQ(count, 3);
}

// edge tests
TYPED_TEST(GraphFixture, edge0)
{
    using graph_type = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using edge_descriptor = typename TestFixture::edge_descriptor;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);

    add_edge(vdA, vdB, g);

    pair<edge_descriptor, bool> e1 = edge(vdA, vdB, g);
    pair<edge_descriptor, bool> e2 = edge(vdB, vdA, g);

    ASSERT_TRUE(e1.second);
    ASSERT_FALSE(e2.second);
    ASSERT_NE(e1.first, e2.first);
}

TYPED_TEST(GraphFixture, edge1)
{
    using graph_type = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using edge_descriptor = typename TestFixture::edge_descriptor;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);

    add_edge(vdA, vdB, g);
    add_edge(vdB, vdC, g);

    pair<edge_descriptor, bool> e1 = edge(vdA, vdB, g);
    pair<edge_descriptor, bool> e2 = edge(vdB, vdC, g);
    pair<edge_descriptor, bool> e3 = edge(vdA, vdC, g);

    ASSERT_TRUE(e1.second);
    ASSERT_TRUE(e2.second);
    ASSERT_FALSE(e3.second);
}

// causes seg fault
// TYPED_TEST(GraphFixture, edge2)
// {
//     using graph_type = typename TestFixture::graph_type;
//     using vertex_descriptor = typename TestFixture::vertex_descriptor;
//     using edge_descriptor = typename TestFixture::edge_descriptor;

//     graph_type g;

//     vertex_descriptor vdA = add_vertex(g);
//     vertex_descriptor vdB = add_vertex(g);

//     pair<edge_descriptor, bool> e1 = edge(vdA, vdB, g);

//     add_edge(vdA, vdB, g);

//     pair<edge_descriptor, bool> e2 = edge(vdA, vdB, g);

//     ASSERT_EQ(e1.first, e2.first);
//     cout << '4' << endl;
//     ASSERT_FALSE(e1.second);
//     cout << '5' << endl;
//     ASSERT_TRUE(e2.second);
//     cout << '6' << endl;
// }

// edges tests
TYPED_TEST(GraphFixture, edges0)
{
    using graph_type = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using edge_iterator = typename TestFixture::edge_iterator;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);
    vertex_descriptor vdD = add_vertex(g);

    add_edge(vdA, vdB, g);
    add_edge(vdA, vdC, g);
    add_edge(vdA, vdD, g);
    add_edge(vdB, vdC, g);
    add_edge(vdB, vdD, g);

    pair<edge_iterator, edge_iterator> edge_list = edges(g);
    edge_iterator b = edge_list.first;
    edge_iterator e = edge_list.second;
    int count = 0;
    while (b != e)
    {
        ++count;
        ++b;
    }
    ASSERT_EQ(count, 5);
}

TYPED_TEST(GraphFixture, edges1)
{
    using graph_type = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using edge_iterator = typename TestFixture::edge_iterator;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);

    add_edge(vdA, vdB, g);
    add_edge(vdA, vdB, g);
    add_edge(vdA, vdB, g);
    add_edge(vdA, vdB, g);

    pair<edge_iterator, edge_iterator> edge_list = edges(g);
    edge_list.first++;
    ASSERT_EQ(edge_list.first, edge_list.second);
}

TYPED_TEST(GraphFixture, edges2)
{
    using graph_type = typename TestFixture::graph_type;
    using edge_iterator = typename TestFixture::edge_iterator;

    graph_type g;

    add_vertex(g);
    add_vertex(g);

    pair<edge_iterator, edge_iterator> edge_list = edges(g);
    edge_iterator b = edge_list.first;
    edge_iterator e = edge_list.second;
    ASSERT_EQ(b, e);
}

// num_edges tests
TYPED_TEST(GraphFixture, num_edges0)
{
    using graph_type = typename TestFixture::graph_type;

    graph_type g;

    add_vertex(g);
    add_vertex(g);

    ASSERT_EQ(num_edges(g), 0);
}

TYPED_TEST(GraphFixture, num_edges1)
{
    using graph_type = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);

    add_edge(vdA, vdB, g);
    add_edge(vdB, vdA, g);
    add_edge(vdA, vdC, g);
    add_edge(vdB, vdC, g);
    add_edge(vdA, vdB, g);
    add_edge(vdB, vdA, g);
    add_edge(vdA, vdC, g);
    add_edge(vdB, vdC, g);

    ASSERT_EQ(num_edges(g), 4);
}

TYPED_TEST(GraphFixture, num_edges2)
{
    using graph_type = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;

    graph_type g;

    vertex_descriptor vertex_array[150];
    for (int i = 0; i < 150; ++i)
    {
        vertex_array[i] = add_vertex(g);
        add_edge(vertex_array[i], vertex_array[i / 2], g);
    }

    ASSERT_EQ(num_edges(g), 150);
}

// num_vertices tests
TYPED_TEST(GraphFixture, num_vertices0)
{
    using graph_type = typename TestFixture::graph_type;

    graph_type g;

    for (int i = 0; i < 150; ++i)
    {
        add_vertex(g);
    }

    ASSERT_EQ(num_vertices(g), 150);
}

TYPED_TEST(GraphFixture, num_vertices1)
{
    using graph_type = typename TestFixture::graph_type;

    graph_type g;

    ASSERT_EQ(num_vertices(g), 0);
}

// source tests
TYPED_TEST(GraphFixture, source0)
{
    using graph_type = typename TestFixture::graph_type;
    using edge_descriptor = typename TestFixture::edge_descriptor;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);

    pair<edge_descriptor, bool> e1 = add_edge(vdA, vdB, g);
    pair<edge_descriptor, bool> e2 = add_edge(vdB, vdA, g);

    ASSERT_EQ(source(e1.first, g), vdA);
    ASSERT_EQ(source(e2.first, g), vdB);
}

TYPED_TEST(GraphFixture, source1)
{
    using graph_type = typename TestFixture::graph_type;
    using edge_descriptor = typename TestFixture::edge_descriptor;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);

    pair<edge_descriptor, bool> e1 = add_edge(vdA, vdB, g);
    add_edge(vdA, vdB, g);

    ASSERT_EQ(source(e1.first, g), vdA);
}

TYPED_TEST(GraphFixture, source2)
{
    using graph_type = typename TestFixture::graph_type;
    using edge_descriptor = typename TestFixture::edge_descriptor;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);

    add_edge(vdA, vdB, g);

    pair<edge_descriptor, bool> e1 = edge(vdA, vdB, g);
    ASSERT_EQ(source(e1.first, g), vdA);
}

// target tests
TYPED_TEST(GraphFixture, target0)
{
    using graph_type = typename TestFixture::graph_type;
    using edge_descriptor = typename TestFixture::edge_descriptor;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);

    pair<edge_descriptor, bool> e1 = add_edge(vdA, vdB, g);
    pair<edge_descriptor, bool> e2 = add_edge(vdB, vdA, g);

    ASSERT_EQ(target(e1.first, g), vdB);
    ASSERT_EQ(target(e2.first, g), vdA);
}

TYPED_TEST(GraphFixture, target1)
{
    using graph_type = typename TestFixture::graph_type;
    using edge_descriptor = typename TestFixture::edge_descriptor;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);

    pair<edge_descriptor, bool> e1 = add_edge(vdA, vdB, g);
    add_edge(vdA, vdB, g);

    ASSERT_EQ(target(e1.first, g), vdB);
}

TYPED_TEST(GraphFixture, target2)
{
    using graph_type = typename TestFixture::graph_type;
    using edge_descriptor = typename TestFixture::edge_descriptor;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);

    add_edge(vdA, vdB, g);

    pair<edge_descriptor, bool> e1 = edge(vdA, vdB, g);
    ASSERT_EQ(target(e1.first, g), vdB);
}

// vertex tests
TYPED_TEST(GraphFixture, vertex0)
{
    using graph_type = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);

    ASSERT_EQ(vertex(0, g), vdA);
    ASSERT_EQ(vertex(1, g), vdB);
    ASSERT_EQ(vertex(2, g), vdC);
}

TYPED_TEST(GraphFixture, vertex1)
{
    using graph_type = typename TestFixture::graph_type;

    graph_type g;

    // ASSERT_EQ(vertex(-1, g), 0); undefined behavior
    ASSERT_EQ(vertex(0, g), 0); // returns number asked for if vertex dne
    ASSERT_EQ(vertex(1, g), 1);
    ASSERT_EQ(vertex(2, g), 2);
}

TYPED_TEST(GraphFixture, vertex2)
{
    using graph_type = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);

    ASSERT_EQ(vertex(0, g), vdA);
    ASSERT_EQ(vertex(1, g), vdB);
    ASSERT_EQ(vertex(2, g), vdC);
    ASSERT_EQ(vertex(3, g), 3);
    ASSERT_EQ(vertex(4, g), 4);
}

TYPED_TEST(GraphFixture, addedge0)
{
    using graph_type = typename TestFixture::graph_type;
    using edge_descriptor = typename TestFixture::edge_descriptor;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;

    graph_type g;

    std::pair<edge_descriptor, bool> ed = add_edge(0, 1, g);

    EXPECT_TRUE(ed.second);

    vertex_descriptor v1 = vertex(0, g);
    vertex_descriptor v2 = vertex(1, g);

    std::pair<edge_descriptor, bool> e1 = edge(v1, v2, g);
    EXPECT_TRUE(e1.second);
}

TYPED_TEST(GraphFixture, addedge1)
{
    using graph_type = typename TestFixture::graph_type;
    using edge_descriptor = typename TestFixture::edge_descriptor;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);

    std::pair<edge_descriptor, bool> p = add_edge(vdA, 1, g);

    EXPECT_TRUE(p.second);
}

TYPED_TEST(GraphFixture, addedge3)
{
    using graph_type = typename TestFixture::graph_type;
    using edge_descriptor = typename TestFixture::edge_descriptor;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);

    std::pair<edge_descriptor, bool> p = add_edge(vdA, -1, g);

    EXPECT_TRUE(p.second);
}

TYPED_TEST(GraphFixture, addedge4)
{
    using graph_type = typename TestFixture::graph_type;
    using edge_descriptor = typename TestFixture::edge_descriptor;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;

    graph_type g;

    vertex_descriptor v1 = add_vertex(g);
    vertex_descriptor v2 = add_vertex(g);

    std::pair<edge_descriptor, bool> e1 = add_edge(v1, v2, g);
    EXPECT_TRUE(e1.second);

    std::pair<edge_descriptor, bool> e2 = add_edge(v1, v2, g);
    EXPECT_FALSE(e2.second);
}

TYPED_TEST(GraphFixture, addedge6)
{
    using graph_type = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using edge_descriptor = typename TestFixture::edge_descriptor;

    graph_type g;

    vertex_descriptor v1 = add_vertex(g);
    vertex_descriptor v2 = add_vertex(g);

    add_edge(v1, v2, g);

    ASSERT_EQ(num_vertices(g), 2);

    std::pair<edge_descriptor, bool> e1 = edge(v1, v2, g);
    EXPECT_TRUE(e1.second);
}

// ----------
// add_vertex
// ----------

TYPED_TEST(GraphFixture, addvertex0)
{
    using graph_type = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;

    graph_type g;

    std::unordered_set<vertex_descriptor> unique_vertex_descriptors;

    for (int i = 0; i < 10000; ++i)
    {
        vertex_descriptor vd = add_vertex(g);
        EXPECT_TRUE(unique_vertex_descriptors.insert(vd).second);
    }
}

TYPED_TEST(GraphFixture, addvertex1)
{
    using graph_type = typename TestFixture::graph_type;

    size_t expected_num_vertices = 0;
    graph_type g;
    ASSERT_EQ(num_vertices(g), expected_num_vertices);

    for (int i = 0; i < 10000; ++i)
    {
        add_vertex(g);
        ++expected_num_vertices;
        ASSERT_EQ(num_vertices(g), expected_num_vertices);
    }
}

// -----------------
// adjacent_vertices
// -----------------

TYPED_TEST(GraphFixture, adj0)
{
    using graph_type = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);

    add_edge(vdA, vdB, g);
    add_edge(vdA, vdC, g);

    ASSERT_EQ(num_vertices(g), 3);

    adjacent_vertices(1000, g);

    ASSERT_EQ(num_vertices(g), 3);
}

TYPED_TEST(GraphFixture, adj1)
{
    using graph_type = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using adjacency_iterator = typename TestFixture::adjacency_iterator;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);

    pair<adjacency_iterator, adjacency_iterator> p = adjacent_vertices(vdA, g);
    adjacency_iterator b = p.first;
    adjacency_iterator e = p.second;
    // vdA does not have neighbors
    ASSERT_EQ(b, e);
}

TYPED_TEST(GraphFixture, edgee1)
{
    using graph_type = typename TestFixture::graph_type;
    using edge_descriptor = typename TestFixture::edge_descriptor;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;

    graph_type g;

    vertex_descriptor v1 = add_vertex(g);
    vertex_descriptor v2 = add_vertex(g);

    add_edge(v1, v2, g);
    add_edge(v2, v1, g);

    std::pair<edge_descriptor, bool> e1 = edge(v1, v2, g);
    EXPECT_TRUE(e1.second);

    std::pair<edge_descriptor, bool> e2 = edge(v2, v1, g);
    EXPECT_TRUE(e2.second);
}

TYPED_TEST(GraphFixture, numedges0)
{
    using graph_type = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;

    graph_type g;

    ASSERT_EQ(0, num_edges(g));

    vertex_descriptor v1 = add_vertex(g);
    vertex_descriptor v2 = add_vertex(g);

    add_edge(v1, v2, g);

    ASSERT_EQ(1, num_edges(g));

    add_edge(v1, v2, g);

    ASSERT_EQ(1, num_edges(g));
}

TYPED_TEST(GraphFixture, numedges1)
{
    using graph_type = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;

    graph_type g;

    ASSERT_EQ(0, num_edges(g));

    vertex_descriptor v1 = add_vertex(g);
    vertex_descriptor v2 = add_vertex(g);
    vertex_descriptor v3 = add_vertex(g);

    add_edge(v1, v2, g);
    ASSERT_EQ(1, num_edges(g));

    add_edge(v1, v3, g);
    ASSERT_EQ(2, num_edges(g));

    add_edge(v2, v1, g);
    ASSERT_EQ(3, num_edges(g));

    add_edge(v2, v3, g);
    ASSERT_EQ(4, num_edges(g));

    add_edge(v3, v1, g);
    ASSERT_EQ(5, num_edges(g));

    add_edge(v3, v2, g);
    ASSERT_EQ(6, num_edges(g));
}

TYPED_TEST(GraphFixture, numedges2)
{
    using graph_type = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;

    graph_type g;

    ASSERT_EQ(0, num_edges(g));

    vertex_descriptor v1 = add_vertex(g);
    vertex_descriptor v2 = add_vertex(g);
    vertex_descriptor v3 = add_vertex(g);

    add_edge(v1, v1, g);
    ASSERT_EQ(1, num_edges(g));

    add_edge(v2, v2, g);
    ASSERT_EQ(2, num_edges(g));

    add_edge(v3, v3, g);
    ASSERT_EQ(3, num_edges(g));
}

// ------------
// num_vertices
// ------------

TYPED_TEST(GraphFixture, numvertices0)
{
    using graph_type = typename TestFixture::graph_type;

    graph_type g;
    ASSERT_EQ(0, num_vertices(g));

    add_vertex(g);
    ASSERT_EQ(1, num_vertices(g));

    add_vertex(g);
    ASSERT_EQ(2, num_vertices(g));
}

// ------
// source
// ------

TYPED_TEST(GraphFixture, sourcee0)
{
    using graph_type = typename TestFixture::graph_type;
    using edge_descriptor = typename TestFixture::edge_descriptor;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;

    graph_type g;

    vertex_descriptor v1 = add_vertex(g);
    vertex_descriptor v2 = add_vertex(g);

    std::pair<edge_descriptor, bool> e1 = add_edge(v1, v2, g);
    std::pair<edge_descriptor, bool> e2 = add_edge(v2, v1, g);

    EXPECT_TRUE(e1.second);
    EXPECT_TRUE(e2.second);

    ASSERT_EQ(source(e1.first, g), v1);
    ASSERT_EQ(source(e2.first, g), v2);
}

TYPED_TEST(GraphFixture, sourcee1)
{
    using graph_type = typename TestFixture::graph_type;
    using edge_descriptor = typename TestFixture::edge_descriptor;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;

    graph_type g;

    vertex_descriptor v1 = add_vertex(g);

    std::pair<edge_descriptor, bool> e1 = add_edge(v1, v1, g);

    EXPECT_TRUE(e1.second);

    ASSERT_EQ(source(e1.first, g), v1);
}

// ------
// target
// ------

TYPED_TEST(GraphFixture, targett0)
{
    using graph_type = typename TestFixture::graph_type;
    using edge_descriptor = typename TestFixture::edge_descriptor;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;

    graph_type g;

    vertex_descriptor v1 = add_vertex(g);
    vertex_descriptor v2 = add_vertex(g);

    std::pair<edge_descriptor, bool> e1 = add_edge(v1, v2, g);
    std::pair<edge_descriptor, bool> e2 = add_edge(v2, v1, g);

    EXPECT_TRUE(e1.second);
    EXPECT_TRUE(e2.second);

    ASSERT_EQ(target(e1.first, g), v2);
    ASSERT_EQ(target(e2.first, g), v1);
}

TYPED_TEST(GraphFixture, targett1)
{
    using graph_type = typename TestFixture::graph_type;
    using edge_descriptor = typename TestFixture::edge_descriptor;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;

    graph_type g;

    vertex_descriptor v1 = add_vertex(g);

    std::pair<edge_descriptor, bool> e1 = add_edge(v1, v1, g);

    EXPECT_TRUE(e1.second);

    ASSERT_EQ(target(e1.first, g), v1);
}

// ------
// vertex
// ------

TYPED_TEST(GraphFixture, vertexx0)
{
    using graph_type = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;

    graph_type g;

    vertex_descriptor v = vertex(2, g);

    ASSERT_EQ(v, 2);

    ASSERT_EQ(num_vertices(g), 0);
}

TYPED_TEST(GraphFixture, vertexx1)
{
    using graph_type = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;

    graph_type g;

    vertex_descriptor v1 = add_vertex(g);

    vertex_descriptor v2 = vertex(v1, g);

    ASSERT_EQ(v1, v2);
}
