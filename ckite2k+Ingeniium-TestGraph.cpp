// -------------
// TestGraph.c++
// -------------

// https://www.boost.org/doc/libs/1_73_0/libs/graph/doc/index.html

// --------
// includes
// --------

#include <iostream> // cout, endl
#include <iterator> // ostream_iterator
#include <sstream>  // ostringstream
#include <utility>  // pair

#include "boost/graph/adjacency_list.hpp" // adjacency_list

#include "gtest/gtest.h"

#include "Graph.hpp"

// ------
// usings
// ------

using namespace std;
using namespace testing;

// ---------
// TestGraph
// ---------

template <typename G>
struct GraphFixture : Test {
    // ------
    // usings
    // ------

    using graph_type          = G;
    using vertex_descriptor   = typename G::vertex_descriptor;
    using edge_descriptor     = typename G::edge_descriptor;
    using vertex_iterator     = typename G::vertex_iterator;
    using edge_iterator       = typename G::edge_iterator;
    using adjacency_iterator  = typename G::adjacency_iterator;
    using vertices_size_type  = typename G::vertices_size_type;
    using edges_size_type     = typename G::edges_size_type;};

// directed, sparse, unweighted
// possibly connected
// possibly cyclic
using
    graph_types =
    Types<
          boost::adjacency_list<boost::setS, boost::vecS, boost::directedS>,
          Graph // uncomment
            >;

#ifdef __APPLE__
    TYPED_TEST_CASE(GraphFixture, graph_types,);
#else
    TYPED_TEST_CASE(GraphFixture, graph_types);
#endif

TYPED_TEST(GraphFixture, test0) {
    using graph_type         = typename TestFixture::graph_type;
    using vertex_descriptor  = typename TestFixture::vertex_descriptor;
    using vertices_size_type = typename TestFixture::vertices_size_type;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);

    vertex_descriptor vd = vertex(0, g);
    ASSERT_EQ(vd, vdA);

    vertices_size_type vs = num_vertices(g);
    ASSERT_EQ(vs, 1u);}

TYPED_TEST(GraphFixture, test1) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using edge_descriptor   = typename TestFixture::edge_descriptor;
    using edges_size_type   = typename TestFixture::edges_size_type;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);

    edge_descriptor edAB = add_edge(vdA, vdB, g).first;
    //std::cout << num_edges(g) << std::endl;
    pair<edge_descriptor, bool> p1 = add_edge(vdA, vdB, g);
    //std::cout << num_edges(g) << std::endl;
    ASSERT_EQ(p1.first,  edAB);
    ASSERT_EQ(p1.second, false);

    pair<edge_descriptor, bool> p2 = edge(vdA, vdB, g);
    ASSERT_EQ(p2.first,  edAB);
    ASSERT_EQ(p2.second, true);

    edges_size_type es = num_edges(g);
    ASSERT_EQ(es, 1u);

    vertex_descriptor vd1 = source(edAB, g);
    ASSERT_EQ(vd1, vdA);

    vertex_descriptor vd2 = target(edAB, g);
    ASSERT_EQ(vd2, vdB);}

TYPED_TEST(GraphFixture, test2) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using vertex_iterator   = typename TestFixture::vertex_iterator;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);

    pair<vertex_iterator, vertex_iterator> p = vertices(g);
    vertex_iterator                        b = p.first;
    vertex_iterator                        e = p.second;
    ASSERT_NE(b, e);

    vertex_descriptor vd1 = *b;
    ASSERT_EQ(vd1, vdA);
    ++b;
    ASSERT_NE(b, e);

    vertex_descriptor vd2 = *b;
    ASSERT_EQ(vd2, vdB);
    ++b;
    ASSERT_EQ(b, e);}

TYPED_TEST(GraphFixture, test3) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using edge_descriptor   = typename TestFixture::edge_descriptor;
    using edge_iterator     = typename TestFixture::edge_iterator;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);

    edge_descriptor edAB = add_edge(vdA, vdB, g).first;
    edge_descriptor edAC = add_edge(vdA, vdC, g).first;

    pair<edge_iterator, edge_iterator> p = edges(g);
    edge_iterator                      b = p.first;
    edge_iterator                      e = p.second;
    ASSERT_NE(b, e);

    edge_descriptor ed1 = *b;
    ASSERT_EQ(ed1, edAB);
    ++b;
    ASSERT_NE(b, e);

    edge_descriptor ed2 = *b;
    ASSERT_EQ(ed2, edAC);
    ++b;
    ASSERT_EQ(e, b);}

    TYPED_TEST(GraphFixture, add_vertex1) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);
    
    ASSERT_EQ(num_vertices(g), 3);
 
    }
    
    TYPED_TEST(GraphFixture, add_vertex2) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);
    
    ASSERT_EQ(vdA, 0);
    ASSERT_EQ(vdB, 1);
    ASSERT_EQ(vdC, 2);
 
    }
    
    TYPED_TEST(GraphFixture, add_vertex3) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;

    graph_type g;
    srand(time(0));
    for(int i = 0;i < 100; ++i)
        add_vertex(g);
    ASSERT_EQ(num_vertices(g), 100);
 
    }
    
    TYPED_TEST(GraphFixture, add_vertex4) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;

    graph_type g;
    srand(time(0));
    for(int i = 0;i < 1000; ++i)
        add_vertex(g);
    ASSERT_EQ(num_vertices(g), 1000);
 
    }
    
    TYPED_TEST(GraphFixture, add_vertex5) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;
    vertex_descriptor vdA[100];
    graph_type g;
    srand(time(0));
    for(int i = 0;i < 100; ++i)
        vdA[i] = add_vertex(g);
    ASSERT_EQ(num_vertices(g), 100);
 
    }
    
    TYPED_TEST(GraphFixture, add_vertex6) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;

    graph_type g;
    srand(time(0));
    for(int i = 0;i < 999999; ++i)
        add_vertex(g);
    ASSERT_EQ(num_vertices(g), 999999);
 
    }
    
    TYPED_TEST(GraphFixture, add_vertex7) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;
    vertex_descriptor vdA[100];
    graph_type g;
    srand(time(0));
    for(int i = 0;i < 100; ++i)
        vdA[i] = add_vertex(g);
    for(int i = 0;i < 100; ++i)
        ASSERT_EQ(vdA[i], i);
    }
    
    TYPED_TEST(GraphFixture, add_edge1) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);
    
    add_edge(vdA, vdA, g);
    add_edge(vdB, vdB, g);
    add_edge(vdC, vdC, g);
    
    ASSERT_EQ(num_edges(g), 3); 
    }
    
    TYPED_TEST(GraphFixture, add_edge2) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);
    
     for(int i = 0;i < 100; ++i)
        add_edge(rand() % num_vertices(g), rand() % num_vertices(g), g);
    ASSERT_LE(num_edges(g), 9);
    
    }
    
     TYPED_TEST(GraphFixture, add_edge3) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;

    graph_type g;
    
     for(int i = 0;i < 100; ++i)
        for(int j = 100; j < 110; ++j)
            add_edge(i, j, g);
    ASSERT_EQ(num_vertices(g), 110);
    ASSERT_EQ(num_edges(g), 1000); 
    
    }
    
     TYPED_TEST(GraphFixture, edge1) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);
    
    add_edge(vdA, vdA, g);
    add_edge(vdB, vdB, g);
    add_edge(vdC, vdC, g);
    
    ASSERT_TRUE(edge(vdA, vdA, g).second);
    ASSERT_TRUE(edge(vdB, vdB, g).second);
    ASSERT_TRUE(edge(vdC, vdC, g).second);
    ASSERT_FALSE(edge(vdA, vdC, g).second);
    
    }
    
    TYPED_TEST(GraphFixture, edge2) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);
    
    add_edge(vdA, vdB, g);
    add_edge(vdB, vdC, g);
    add_edge(vdC, vdA, g);
    
    ASSERT_TRUE(edge(vdA, vdB, g).second);
    ASSERT_TRUE(edge(vdB, vdC, g).second);
    ASSERT_TRUE(edge(vdC, vdA, g).second);
    ASSERT_FALSE(edge(vdB, vdA, g).second);
    ASSERT_FALSE(edge(vdC, vdB, g).second);
    ASSERT_FALSE(edge(vdA, vdC, g).second);
    
    }
    
    TYPED_TEST(GraphFixture, edge3) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;
    using edge_descriptor   = typename TestFixture::edge_descriptor;
    graph_type g;

    for(int i = 0;i < 100; ++i)
        for(int j = 100; j < 110; ++j)
            add_edge(i, j, g);
            
    for(int i = 0;i < 100; ++i)
        for(int j = 100; j < 110; ++j)
        {
            std::pair<edge_descriptor, bool> p = edge(i, j, g);
            ASSERT_TRUE(p.second);
            //ASSERT_EQ(source(p.first, j - 100 + i * 10);
        }
    }
    
    
    TYPED_TEST(GraphFixture, edge4) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);
    
    add_edge(vdB, vdA, g);
    add_edge(vdC, vdB, g);
    add_edge(vdA, vdC, g);
    
    ASSERT_FALSE(edge(vdA, vdB, g).second);
    ASSERT_FALSE(edge(vdB, vdC, g).second);
    ASSERT_FALSE(edge(vdC, vdA, g).second);
    ASSERT_TRUE(edge(vdB, vdA, g).second);
    ASSERT_TRUE(edge(vdC, vdB, g).second);
    ASSERT_TRUE(edge(vdA, vdC, g).second);
    
    }
    
    TYPED_TEST(GraphFixture, edge5) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);
    
    add_edge(vdB, vdA, g);
    add_edge(vdC, vdB, g);
    add_edge(vdA, vdC, g);
    add_edge(vdA, vdB, g);
    add_edge(vdB, vdC, g);
    add_edge(vdC, vdA, g);
    
    ASSERT_TRUE(edge(vdA, vdB, g).second);
    ASSERT_TRUE(edge(vdB, vdC, g).second);
    ASSERT_TRUE(edge(vdC, vdA, g).second);
    ASSERT_TRUE(edge(vdB, vdA, g).second);
    ASSERT_TRUE(edge(vdC, vdB, g).second);
    ASSERT_TRUE(edge(vdA, vdC, g).second);
    
    }
    
    TYPED_TEST(GraphFixture, edge6) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);
    
    add_edge(vdA, vdA, g);
    add_edge(vdB, vdB, g);
    add_edge(vdC, vdC, g);
    
    ASSERT_FALSE(edge(vdA, vdB, g).second);
    ASSERT_FALSE(edge(vdB, vdC, g).second);
    ASSERT_FALSE(edge(vdC, vdA, g).second);
    ASSERT_FALSE(edge(vdB, vdA, g).second);
    ASSERT_FALSE(edge(vdC, vdB, g).second);
    ASSERT_FALSE(edge(vdA, vdC, g).second);
    
    }
    
    TYPED_TEST(GraphFixture, source1) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;
    using edge_descriptor   = typename TestFixture::edge_descriptor;
    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);
    
    edge_descriptor ed1 = add_edge(vdA, vdA, g).first;
    edge_descriptor ed2 = add_edge(vdB, vdB, g).first;
    edge_descriptor ed3 = add_edge(vdC, vdC, g).first;
    
    ASSERT_EQ(source(ed1, g), vdA);
    ASSERT_EQ(source(ed2, g), vdB);
    ASSERT_EQ(source(ed3, g), vdC);
    
    
    }
    
    TYPED_TEST(GraphFixture, target1) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;
    using edge_descriptor   = typename TestFixture::edge_descriptor;
    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);
    
    edge_descriptor ed1 = add_edge(vdA, vdA, g).first;
    edge_descriptor ed2 = add_edge(vdB, vdB, g).first;
    edge_descriptor ed3 = add_edge(vdC, vdC, g).first;
    
    ASSERT_EQ(target(ed1, g), vdA);
    ASSERT_EQ(target(ed2, g), vdB);
    ASSERT_EQ(target(ed3, g), vdC);
    
    
    }
    
    TYPED_TEST(GraphFixture, source2) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;
    using edge_descriptor   = typename TestFixture::edge_descriptor;
    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);
    
    auto ed1 = add_edge(vdA, vdB, g).first;
    auto ed2 = add_edge(vdB, vdC, g).first;
    auto ed3 = add_edge(vdC, vdA, g).first;
    
    ASSERT_EQ(source(ed1, g), vdA);
    ASSERT_EQ(source(ed2, g), vdB);
    ASSERT_EQ(source(ed3, g), vdC);

    
    }
    
    TYPED_TEST(GraphFixture, target2) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;
    using edge_descriptor   = typename TestFixture::edge_descriptor;
    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);
    
    auto ed1 = add_edge(vdA, vdB, g).first;
    auto ed2 = add_edge(vdB, vdC, g).first;
    auto ed3 = add_edge(vdC, vdA, g).first;
    
    ASSERT_EQ(target(ed1, g), vdB);
    ASSERT_EQ(target(ed2, g), vdC);
    ASSERT_EQ(target(ed3, g), vdA);
    
    }
    
    TYPED_TEST(GraphFixture, source3) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;
    using edge_descriptor   = typename TestFixture::edge_descriptor;
    graph_type g;

    edge_descriptor eds[100];
    for(int i = 0; i < 100; ++i)
        eds[i] = add_edge(i, 24, g).first;
    
    for(int i = 0; i < 100; ++i)
    {
        ASSERT_EQ(source(eds[i], g), i);
    }
  
    }
    
    TYPED_TEST(GraphFixture, target3) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;
    using edge_descriptor   = typename TestFixture::edge_descriptor;
    graph_type g;

    edge_descriptor eds[100];
    for(int i = 0; i < 100; ++i)
        eds[i] = add_edge(i, 24, g).first;
    
    for(int i = 0; i < 100; ++i)
    {
        ASSERT_EQ(target(eds[i], g), 24);
    }
  
    }
    
    TYPED_TEST(GraphFixture, aource_target1) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;
    using edge_descriptor   = typename TestFixture::edge_descriptor;
    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);
    
    edge_descriptor ed1 = add_edge(vdA, vdA, g).first;
    edge_descriptor ed2 = add_edge(vdB, vdB, g).first;
    edge_descriptor ed3 = add_edge(vdC, vdC, g).first;
    
    ASSERT_NE(source(ed1, g), vdB);
    ASSERT_NE(source(ed2, g), vdC);
    ASSERT_NE(source(ed3, g), vdA);  
    }
    
    TYPED_TEST(GraphFixture, source_target2) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;
    using edge_descriptor   = typename TestFixture::edge_descriptor;
    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);
    
    auto ed1 = add_edge(vdA, vdB, g).first;
    auto ed2 = add_edge(vdB, vdC, g).first;
    auto ed3 = add_edge(vdC, vdA, g).first;
    
    ASSERT_NE(source(ed1, g), vdB);
    ASSERT_NE(source(ed2, g), vdC);
    ASSERT_NE(source(ed3, g), vdA);
    ASSERT_NE(target(ed1, g), vdC);
    ASSERT_NE(target(ed2, g), vdA);
    ASSERT_NE(target(ed3, g), vdB);
    
    }
    
    
    TYPED_TEST(GraphFixture, source_target3) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;
    using edge_descriptor   = typename TestFixture::edge_descriptor;
    graph_type g;

    edge_descriptor eds[100];
    for(int i = 0; i < 100; ++i)
        eds[i] = add_edge(i, 101, g).first;
    
    for(int i = 0; i < 100; ++i)
    {
        ASSERT_NE(source(eds[i], g), 101);
        ASSERT_NE(target(eds[i], g), i);
    }
  
    }
    
    TYPED_TEST(GraphFixture, source_target4) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;
    using edge_descriptor   = typename TestFixture::edge_descriptor;
    graph_type g;

    edge_descriptor eds[100];
    for(int i = 0; i < 100; ++i)
        eds[i] = add_edge(i, 101, g).first;
    
    for(int i = 0; i < 100; ++i)
    {
        ASSERT_EQ(source(eds[i], g), i);
        ASSERT_EQ(target(eds[i], g), 101);
    }
  
    }
    
