// -------------
// TestGraph.c++
// -------------

// https://www.boost.org/doc/libs/1_73_0/libs/graph/doc/index.html

// --------
// includes
// --------

#include <iostream> // cout, endl
#include <iterator> // ostream_iterator
#include <sstream>  // ostringstream
#include <utility>  // pair
#include <set>
#include "boost/graph/adjacency_list.hpp" // adjacency_list

#include "gtest/gtest.h"

#include "Graph.hpp"

// ------
// usings
// ------

using namespace std;
using namespace testing;

// ---------
// TestGraph
// ---------

template <typename G>
struct GraphFixture : Test {
    // ------
    // usings
    // ------

    using graph_type          = G;
    using vertex_descriptor   = typename G::vertex_descriptor;
    using edge_descriptor     = typename G::edge_descriptor;
    using vertex_iterator     = typename G::vertex_iterator;
    using edge_iterator       = typename G::edge_iterator;
    using adjacency_iterator  = typename G::adjacency_iterator;
    using vertices_size_type  = typename G::vertices_size_type;
    using edges_size_type     = typename G::edges_size_type;};

// directed, sparse, unweighted
// possibly connected
// possibly cyclic
using
    graph_types =
    Types<
            boost::adjacency_list<boost::setS, boost::vecS, boost::directedS>,
            Graph 
            >;

#ifdef __APPLE__
    TYPED_TEST_CASE(GraphFixture, graph_types);
#else
    TYPED_TEST_CASE(GraphFixture, graph_types);
#endif

template <typename II, typename T = typename II::value_type>
set<T> convert_to_set (II b, II e) {
    set<T> result;
    while (b != e) {
        result.insert(*b);
        ++b;
    }
    return result;
}
// --------
// add_edge
// --------
TYPED_TEST(GraphFixture, add_edge1) {
    using graph_type         = typename TestFixture::graph_type;
    using vertex_descriptor  = typename TestFixture::vertex_descriptor;
    using edge_descriptor = typename TestFixture::edge_descriptor;
    using vertex_iterator    = typename TestFixture::vertex_iterator;
    
    graph_type g;

    pair<edge_descriptor, bool> E1 = add_edge(0, 10, g);

    ASSERT_EQ(E1.second, true);//edge created

    set<vertex_descriptor> s = {0,1,2,3,4,5,6,7,8,9,10};

    pair<vertex_iterator, vertex_iterator> p = vertices(g);
    set<vertex_descriptor> our_set = convert_to_set(p.first, p.second);
    ASSERT_EQ(s, our_set);//same vertices

    pair<edge_descriptor,bool> e = edge(0,10,g);
    ASSERT_EQ(e.second,true);//edge found

    edge_descriptor edge_made = e.first;
    ASSERT_EQ(source(edge_made, g), 0);
    ASSERT_EQ(target(edge_made, g), 10);
}

TYPED_TEST(GraphFixture, add_edge2) {
    using graph_type         = typename TestFixture::graph_type;
    using vertex_descriptor  = typename TestFixture::vertex_descriptor;
    using edge_descriptor = typename TestFixture::edge_descriptor;
    
    graph_type g;
    vertex_descriptor vA = add_vertex(g);
    pair<edge_descriptor, bool> E1 = add_edge(vA, vA, g); //vertex with an edge to itself.
    ASSERT_EQ(E1.second, true);
}

TYPED_TEST(GraphFixture, add_edge3) {
    using graph_type         = typename TestFixture::graph_type;
    using vertex_descriptor  = typename TestFixture::vertex_descriptor;
    using edge_descriptor = typename TestFixture::edge_descriptor;
    
    graph_type g;
    vertex_descriptor vA = add_vertex(g);
    vertex_descriptor vB = add_vertex(g);
    pair<edge_descriptor, bool> E1 = add_edge(vA, vB, g); 
    ASSERT_EQ(E1.second, true);
    pair<edge_descriptor, bool> E2 = add_edge(vA, vB, g);
    ASSERT_EQ(E2.second, false); 
}

// --------
// add_vertex
// --------
TYPED_TEST(GraphFixture, add_vertex1) {
    using graph_type         = typename TestFixture::graph_type;
    using vertices_size_type = typename TestFixture::vertices_size_type;
    
    graph_type g;
    vertices_size_type current = num_vertices(g);
    add_vertex(g);
    vertices_size_type new_size = num_vertices(g);
    ASSERT_GT(new_size, current);
}

TYPED_TEST(GraphFixture, add_vertex2) {
    using graph_type         = typename TestFixture::graph_type;
    using vertex_iterator    = typename TestFixture::vertex_iterator;
    
    graph_type g;
    add_vertex(g);
    pair<vertex_iterator, vertex_iterator> p = vertices(g);
    ASSERT_NE(p.first, p.second);
}

TYPED_TEST(GraphFixture, add_vertex3) {
    using graph_type         = typename TestFixture::graph_type;
    using vertex_descriptor  = typename TestFixture::vertex_descriptor;
    using vertex_iterator    = typename TestFixture::vertex_iterator;
    
    graph_type g;
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    set<vertex_descriptor> s = {0,1,2};
    pair<vertex_iterator, vertex_iterator> p = vertices(g);
    set<vertex_descriptor> our_set = convert_to_set(p.first, p.second);
    ASSERT_EQ(s, our_set);
}
// -----------------
// adjacent_vertices
// -----------------
TYPED_TEST(GraphFixture, adjacent_vertices1) {
    using graph_type         = typename TestFixture::graph_type;
    using vertex_descriptor  = typename TestFixture::vertex_descriptor;
    using adjacency_iterator = typename TestFixture::adjacency_iterator;
    
    graph_type g;
    vertex_descriptor vA = add_vertex(g);
    pair<adjacency_iterator, adjacency_iterator> ai = adjacent_vertices(vA, g);
    set<vertex_descriptor> our_set = convert_to_set(ai.first, ai.second);
    ASSERT_EQ(our_set.size(), 0);
}

TYPED_TEST(GraphFixture, adjacent_vertices2) {
    using graph_type         = typename TestFixture::graph_type;
    using vertex_descriptor  = typename TestFixture::vertex_descriptor;
    using adjacency_iterator = typename TestFixture::adjacency_iterator;
    
    graph_type g;
    vertex_descriptor vA = add_vertex(g);
    vertex_descriptor vB = add_vertex(g);
    vertex_descriptor vC = add_vertex(g);
    add_edge(vC, vB, g);
    add_edge(vC, vA, g);
    add_edge(vC, vC, g);
    pair<adjacency_iterator, adjacency_iterator> ai = adjacent_vertices(vC, g);
    set<vertex_descriptor> our_set = convert_to_set(ai.first, ai.second);
    set<vertex_descriptor> s = {0,1,2};
    ASSERT_EQ(our_set, s);
}

TYPED_TEST(GraphFixture, adjacent_vertices3) {
    using graph_type         = typename TestFixture::graph_type;
    using vertex_descriptor  = typename TestFixture::vertex_descriptor;
    using adjacency_iterator = typename TestFixture::adjacency_iterator;
    graph_type g;
    vertex_descriptor vA = add_vertex(g);
    vertex_descriptor vB = add_vertex(g);
    vertex_descriptor vC = add_vertex(g);
    add_edge(vC, vB, g);
    add_edge(vC, vA, g);
    add_edge(vC, vC, g);
    pair<adjacency_iterator, adjacency_iterator> a = adjacent_vertices(vA, g);
    pair<adjacency_iterator, adjacency_iterator> b = adjacent_vertices(vB, g);
    pair<adjacency_iterator, adjacency_iterator> c = adjacent_vertices(vC, g);
    set<vertex_descriptor> adj_a = convert_to_set(a.first, a.second);
    set<vertex_descriptor> adj_b = convert_to_set(b.first, b.second);
    set<vertex_descriptor> adj_c = convert_to_set(c.first, c.second);
    ASSERT_EQ(adj_a.size(),adj_b.size());
    ASSERT_NE(adj_c.size(),adj_b.size());
    ASSERT_EQ(adj_c.size(),3);
}
// ----
// edge
// ----
TYPED_TEST(GraphFixture, edge1) {
    using graph_type         = typename TestFixture::graph_type;
    using vertex_descriptor  = typename TestFixture::vertex_descriptor;
    using edge_descriptor = typename TestFixture::edge_descriptor;
    using vertex_iterator    = typename TestFixture::vertex_iterator;
    graph_type g;
    vertex_descriptor vA = add_vertex(g);
    vertex_descriptor vB = add_vertex(g);
    vertex_descriptor vC = add_vertex(g);
    add_edge(vC, vB, g);
    add_edge(vC, vA, g);
    add_edge(vC, vC, g);
    pair<edge_descriptor, bool> e1 = edge(vC, vB, g);
    pair<edge_descriptor, bool> e2 = edge(vC, vA, g);
    pair<edge_descriptor, bool> e3 = edge(vC, vC, g);
    ASSERT_EQ(e1.second, true);
    ASSERT_EQ(e2.second, true);
    ASSERT_EQ(e3.second, true);
}

TYPED_TEST(GraphFixture, edge2) {
    using graph_type         = typename TestFixture::graph_type;
    using vertex_descriptor  = typename TestFixture::vertex_descriptor;
    using edge_descriptor = typename TestFixture::edge_descriptor;
    using vertex_iterator    = typename TestFixture::vertex_iterator;
    graph_type g;
    vertex_descriptor vA = add_vertex(g);
    vertex_descriptor vB = add_vertex(g);
    add_edge(vA, vB, g);
    pair<edge_descriptor, bool> e1 = edge(vA, vB, g);
    pair<edge_descriptor, bool> e2 = edge(vB, vA, g);
    ASSERT_EQ(e1.second, true);
    ASSERT_EQ(e2.second, false);
}

TYPED_TEST(GraphFixture, edge3) {
    using graph_type         = typename TestFixture::graph_type;
    using vertex_descriptor  = typename TestFixture::vertex_descriptor;
    using edge_descriptor    = typename TestFixture::edge_descriptor;
    using vertex_iterator    = typename TestFixture::vertex_iterator;
    graph_type g;
    vertex_descriptor vA = add_vertex(g);
    vertex_descriptor vB = add_vertex(g);
    vertex_descriptor vC = add_vertex(g);
    add_edge(vA, vB, g);
    add_edge(vA, vC, g);
    pair<edge_descriptor, bool> e1 = edge(vA, vB, g);
    pair<edge_descriptor, bool> e2 = edge(vA, vC, g);
    ASSERT_EQ(source(e1.first,g) , vA);
    ASSERT_EQ(target(e1.first,g) , vB);

    ASSERT_EQ(source(e2.first,g) , vA);
    ASSERT_EQ(target(e2.first,g) , vC);

}
// -----
// edges
// -----
TYPED_TEST(GraphFixture, edges1) {
    using graph_type         = typename TestFixture::graph_type;
    using vertex_descriptor  = typename TestFixture::vertex_descriptor;
    using edge_descriptor    = typename TestFixture::edge_descriptor;
    using vertex_iterator    = typename TestFixture::vertex_iterator;
    using edge_iterator      = typename TestFixture::edge_iterator;
    graph_type g;
    vertex_descriptor vA = add_vertex(g);
    vertex_descriptor vB = add_vertex(g);
    vertex_descriptor vC = add_vertex(g);
    add_edge(vA, vB, g);
    add_edge(vA, vC, g);
    add_edge(vA, vA, g);
    pair<edge_iterator, edge_iterator> current_edges = edges(g);
    set<edge_descriptor> our_set = convert_to_set(current_edges.first, current_edges.second);
    ASSERT_EQ(our_set.size(), 3);
}

TYPED_TEST(GraphFixture, edges2) {
    using graph_type         = typename TestFixture::graph_type;
    using vertex_descriptor  = typename TestFixture::vertex_descriptor;
    using edge_descriptor    = typename TestFixture::edge_descriptor;
    using vertex_iterator    = typename TestFixture::vertex_iterator;
    using edge_iterator      = typename TestFixture::edge_iterator;
    graph_type g;
    vertex_descriptor vA = add_vertex(g);
    vertex_descriptor vB = add_vertex(g);
    vertex_descriptor vC = add_vertex(g);
    pair<edge_descriptor, bool> e1 = add_edge(vA, vB, g);
    pair<edge_descriptor, bool> e2 = add_edge(vA, vC, g);
    pair<edge_descriptor, bool> e3 = add_edge(vA, vA, g);
    pair<edge_iterator, edge_iterator> current_edges = edges(g);
    set<edge_descriptor> our_set = convert_to_set(current_edges.first, current_edges.second);
    ASSERT_TRUE(our_set.find(e1.first) != our_set.end());
    ASSERT_TRUE(our_set.find(e2.first) != our_set.end());
    ASSERT_TRUE(our_set.find(e3.first) != our_set.end());
}

TYPED_TEST(GraphFixture, edges3) {
    using graph_type         = typename TestFixture::graph_type;
    using vertex_descriptor  = typename TestFixture::vertex_descriptor;
    using edge_descriptor    = typename TestFixture::edge_descriptor;
    using vertex_iterator    = typename TestFixture::vertex_iterator;
    using edge_iterator      = typename TestFixture::edge_iterator;
    graph_type g;
    vertex_descriptor vA = add_vertex(g);
    vertex_descriptor vB = add_vertex(g);
    vertex_descriptor vC = add_vertex(g);
    pair<edge_descriptor, bool> e1 = add_edge(vA, vB, g);
    pair<edge_descriptor, bool> e2 = add_edge(vA, vC, g);
    pair<edge_descriptor, bool> e3 = add_edge(vA, vA, g);
    pair<edge_iterator, edge_iterator> current_edges = edges(g);
    set<edge_descriptor> our_set = convert_to_set(current_edges.first, current_edges.second);
    ASSERT_TRUE(e1.second);
    ASSERT_TRUE(e2.second);
    ASSERT_TRUE(e3.second);

}
/*
 *  tests for VERTICES 
 */
TYPED_TEST(GraphFixture, vertices_1) {
    using graph_type         = typename TestFixture::graph_type;
    using vertex_descriptor  = typename TestFixture::vertex_descriptor;
    using vertices_size_type = typename TestFixture::vertices_size_type;
    using edge_descriptor = typename TestFixture::edge_descriptor;
    using vertex_iterator    = typename TestFixture::vertex_iterator;

    graph_type g;
    vertex_descriptor v1 = add_vertex(g);
    
    vertex_iterator b = vertices(g).first;
    vertex_iterator e = vertices(g).second;

    ASSERT_NE(b, e);

    ASSERT_EQ(v1, *b);

    ++b;
    ASSERT_EQ(b, e);
}

TYPED_TEST(GraphFixture, vertices_2) {
    using graph_type         = typename TestFixture::graph_type;
    using vertex_descriptor  = typename TestFixture::vertex_descriptor;
    using vertices_size_type = typename TestFixture::vertices_size_type;
    using edge_descriptor = typename TestFixture::edge_descriptor;
    using vertex_iterator    = typename TestFixture::vertex_iterator;

    graph_type g;
    vertex_descriptor v1 = add_vertex(g);
    vertex_descriptor v2 = add_vertex(g);
    
    vertex_iterator b = vertices(g).first;
    vertex_iterator e = vertices(g).second;

    ASSERT_NE(b, e);

    ASSERT_EQ(v1, *b);

    ++b;
    ASSERT_NE(b, e);

    ASSERT_EQ(v2, *b);

    ++b;
    ASSERT_EQ(b, e);
}

TYPED_TEST(GraphFixture, vertices_3) {
    using graph_type         = typename TestFixture::graph_type;
    using vertex_descriptor  = typename TestFixture::vertex_descriptor;
    using vertices_size_type = typename TestFixture::vertices_size_type;
    using edge_descriptor = typename TestFixture::edge_descriptor;
    using vertex_iterator    = typename TestFixture::vertex_iterator;

    graph_type g;

    pair<edge_descriptor, bool> E1 = add_edge(0, 9, g);
    ASSERT_EQ(E1.second, true);

    set<vertex_descriptor> s = {0,1,2,3,4,5,6,7,8,9};
    set<vertex_descriptor> our_set = convert_to_set(vertices(g).first, vertices(g).second);
    ASSERT_EQ(s, our_set);
}

/*
 *  tests for TARGET 
 */
TYPED_TEST(GraphFixture, target_1) {
    using graph_type         = typename TestFixture::graph_type;
    using vertex_descriptor  = typename TestFixture::vertex_descriptor;
    using vertices_size_type = typename TestFixture::vertices_size_type;
    using edge_descriptor = typename TestFixture::edge_descriptor;
    using vertex_iterator    = typename TestFixture::vertex_iterator;

    graph_type g;
    vertex_descriptor v1 = add_vertex(g);
    vertex_descriptor v2 = add_vertex(g);

    edge_descriptor e1 = add_edge(v1, v2, g).first;

    ASSERT_EQ(target(e1, g), v2);
}

TYPED_TEST(GraphFixture, target_2) {
    using graph_type         = typename TestFixture::graph_type;
    using vertex_descriptor  = typename TestFixture::vertex_descriptor;
    using vertices_size_type = typename TestFixture::vertices_size_type;
    using edge_descriptor = typename TestFixture::edge_descriptor;
    using vertex_iterator    = typename TestFixture::vertex_iterator;

    graph_type g;
    vertex_descriptor v1 = add_vertex(g);
    vertex_descriptor v2 = add_vertex(g);
    vertex_descriptor v3 = add_vertex(g);

    edge_descriptor e1 = add_edge(v1, v2, g).first;
    edge_descriptor e3 = add_edge(v2, v3, g).first;

    ASSERT_EQ(target(e3, g), v3);
}

TYPED_TEST(GraphFixture, target_3) {
    using graph_type         = typename TestFixture::graph_type;
    using vertex_descriptor  = typename TestFixture::vertex_descriptor;
    using vertices_size_type = typename TestFixture::vertices_size_type;
    using edge_descriptor = typename TestFixture::edge_descriptor;
    using vertex_iterator    = typename TestFixture::vertex_iterator;

    graph_type g;
    vertex_descriptor v1 = add_vertex(g);
    vertex_descriptor v2 = add_vertex(g);
    vertex_descriptor v3 = add_vertex(g);

    edge_descriptor e1 = add_edge(v1, v2, g).first;
    edge_descriptor e2 = add_edge(v3, v2, g).first;

    ASSERT_EQ(target(e2, g), v2);
}


/*
 *  tests for SOURCE 
 */
TYPED_TEST(GraphFixture, source_1) {
    using graph_type         = typename TestFixture::graph_type;
    using vertex_descriptor  = typename TestFixture::vertex_descriptor;
    using vertices_size_type = typename TestFixture::vertices_size_type;
    using edge_descriptor = typename TestFixture::edge_descriptor;
    using vertex_iterator    = typename TestFixture::vertex_iterator;

    graph_type g;
    vertex_descriptor v1 = add_vertex(g);
    vertex_descriptor v2 = add_vertex(g);

    edge_descriptor e1 = add_edge(v1, v2, g).first;

    ASSERT_EQ(source(e1, g), v1);
}

TYPED_TEST(GraphFixture, source_2) {
    using graph_type         = typename TestFixture::graph_type;
    using vertex_descriptor  = typename TestFixture::vertex_descriptor;
    using vertices_size_type = typename TestFixture::vertices_size_type;
    using edge_descriptor = typename TestFixture::edge_descriptor;
    using vertex_iterator    = typename TestFixture::vertex_iterator;

    graph_type g;
    vertex_descriptor v1 = add_vertex(g);
    vertex_descriptor v2 = add_vertex(g);
    vertex_descriptor v3 = add_vertex(g);

    edge_descriptor e1 = add_edge(v1, v2, g).first;
    edge_descriptor e3 = add_edge(v2, v3, g).first;

    ASSERT_EQ(source(e3, g), v2);
}

TYPED_TEST(GraphFixture, source_3) {
    using graph_type         = typename TestFixture::graph_type;
    using vertex_descriptor  = typename TestFixture::vertex_descriptor;
    using vertices_size_type = typename TestFixture::vertices_size_type;
    using edge_descriptor = typename TestFixture::edge_descriptor;
    using vertex_iterator    = typename TestFixture::vertex_iterator;

    graph_type g;
    vertex_descriptor v1 = add_vertex(g);
    vertex_descriptor v2 = add_vertex(g);
    vertex_descriptor v3 = add_vertex(g);

    edge_descriptor e1 = add_edge(v1, v2, g).first;
    edge_descriptor e2 = add_edge(v3, v2, g).first;

    ASSERT_EQ(source(e2, g), v3);
}


/*
 *  tests for NUM_VERTICES 
 */
TYPED_TEST(GraphFixture, num_vertices_1) {
    using graph_type         = typename TestFixture::graph_type;
    using vertex_descriptor  = typename TestFixture::vertex_descriptor;
    using vertices_size_type = typename TestFixture::vertices_size_type;
    using edge_descriptor = typename TestFixture::edge_descriptor;
    using vertex_iterator    = typename TestFixture::vertex_iterator;

    graph_type g;
    vertex_descriptor v1 = add_vertex(g);
    vertex_descriptor v2 = add_vertex(g);

    ASSERT_EQ(num_vertices(g), 2);
}

TYPED_TEST(GraphFixture, num_vertices_2) {
    using graph_type         = typename TestFixture::graph_type;
    using vertex_descriptor  = typename TestFixture::vertex_descriptor;
    using vertices_size_type = typename TestFixture::vertices_size_type;
    using edge_descriptor = typename TestFixture::edge_descriptor;
    using vertex_iterator    = typename TestFixture::vertex_iterator;

    graph_type g;
    vertex_descriptor v1 = add_vertex(g);
    vertex_descriptor v2 = add_vertex(g);
    vertex_descriptor v3 = add_vertex(g);
    vertex_descriptor v4 = add_vertex(g);

    ASSERT_EQ(num_vertices(g), 4);
}

TYPED_TEST(GraphFixture, num_vertices_3) {
    using graph_type         = typename TestFixture::graph_type;
    using vertex_descriptor  = typename TestFixture::vertex_descriptor;
    using vertices_size_type = typename TestFixture::vertices_size_type;
    using edge_descriptor = typename TestFixture::edge_descriptor;
    using vertex_iterator    = typename TestFixture::vertex_iterator;

    graph_type g;

    ASSERT_EQ(num_vertices(g), 0);
}


/*
 *  tests for NUM_EDGES 
 */
TYPED_TEST(GraphFixture, num_edges_1) {
    using graph_type         = typename TestFixture::graph_type;
    using vertex_descriptor  = typename TestFixture::vertex_descriptor;
    using vertices_size_type = typename TestFixture::vertices_size_type;
    using edge_descriptor    = typename TestFixture::edge_descriptor;
    using vertex_iterator    = typename TestFixture::vertex_iterator;

    graph_type g;
    vertex_descriptor v1 = add_vertex(g);
    vertex_descriptor v2 = add_vertex(g);

    edge_descriptor e1 = add_edge(v1, v2, g).first;

    ASSERT_EQ(num_edges(g), 1);
}

TYPED_TEST(GraphFixture, num_edges_2) {
    using graph_type         = typename TestFixture::graph_type;
    using vertex_descriptor  = typename TestFixture::vertex_descriptor;
    using vertices_size_type = typename TestFixture::vertices_size_type;
    using edge_descriptor    = typename TestFixture::edge_descriptor;
    using vertex_iterator    = typename TestFixture::vertex_iterator;

    graph_type g;
    vertex_descriptor v1 = add_vertex(g);
    vertex_descriptor v2 = add_vertex(g);

    ASSERT_EQ(num_edges(g), 0);
}

TYPED_TEST(GraphFixture, num_edges_3) {
    using graph_type         = typename TestFixture::graph_type;
    using vertex_descriptor  = typename TestFixture::vertex_descriptor;
    using vertices_size_type = typename TestFixture::vertices_size_type;
    using edge_descriptor    = typename TestFixture::edge_descriptor;
    using vertex_iterator    = typename TestFixture::vertex_iterator;

    graph_type g;
    vertex_descriptor v1 = add_vertex(g);
    vertex_descriptor v2 = add_vertex(g);

    edge_descriptor e1 = add_edge(v1, v2, g).first;
    edge_descriptor e2 = add_edge(v1, v2, g).first;

    ASSERT_EQ(num_edges(g), 1);
}
